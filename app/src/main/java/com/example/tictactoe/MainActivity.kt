package com.example.tictactoe

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import java.sql.Time

class MainActivity : AppCompatActivity() , View.OnClickListener{
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }

    private var playerX: Boolean = true

    private fun init(){
        button00.setOnClickListener(){
            playerSwap(it)
        }
        button01.setOnClickListener(){
            playerSwap(it)
        }
        button02.setOnClickListener(){
            playerSwap(it)
        }
        button10.setOnClickListener(){
            playerSwap(it)
        }
        button11.setOnClickListener(){
            playerSwap(it)
        }
        button12.setOnClickListener(){
            playerSwap(it)
        }
        button20.setOnClickListener(){
            playerSwap(it)
        }
        button21.setOnClickListener(){
            playerSwap(it)
        }
        button22.setOnClickListener(){
            playerSwap(it)
        }
        resetbutton.setOnClickListener(){
            resetButtons()
        }
    }

    private fun resetButtons(){
        reset(button00)
        reset(button01)
        reset(button02)
        reset(button10)
        reset(button11)
        reset(button12)
        reset(button20)
        reset(button21)
        reset(button22)
    }

    private fun playerSwap(view: View){
        var button: Button = view as Button

        if(playerX) {
            button.text = "X"
            playerX = false
        }
        else {
            button.text = "O"
            playerX = true
        }
        button.isClickable = false
        check_tie()
        check_winning()

    }

    private fun check_winning(){
        if( button00.text.toString().isNotEmpty() && button00.text.toString() == button01.text.toString() && button00.text.toString() == button02.text.toString() ||
            button10.text.toString().isNotEmpty() && button10.text.toString() == button11.text.toString() && button10.text.toString() == button12.text.toString() ||
            button20.text.toString().isNotEmpty() && button20.text.toString() == button21.text.toString() && button20.text.toString() == button22.text.toString() ||
            button00.text.toString().isNotEmpty() && button00.text.toString() == button10.text.toString() && button10.text.toString() == button20.text.toString() ||
            button01.text.toString().isNotEmpty() && button01.text.toString() == button11.text.toString() && button01.text.toString() == button21.text.toString() ||
            button02.text.toString().isNotEmpty() && button02.text.toString() == button12.text.toString() && button02.text.toString() == button22.text.toString() ||
            button00.text.toString().isNotEmpty() && button00.text.toString() == button11.text.toString() && button00.text.toString() == button22.text.toString() ||
            button02.text.toString().isNotEmpty() && button02.text.toString() == button11.text.toString() && button02.text.toString() == button20.text.toString()) {
            if(playerX) {
                Toast.makeText(this,"Winner is O", Toast.LENGTH_SHORT).show()
            }
            else{
                Toast.makeText(this,"Winner is X", Toast.LENGTH_SHORT).show()
            }
            resetButtons()
        }
    }

    private fun reset(view: View){
        var button: Button = view as Button
        button.text=""
        button.isClickable = true
        playerX = true
    }

    private fun check_tie(){
        if(button00.text.toString().isNotEmpty()
                && button01.text.toString().isNotEmpty()
                && button02.text.toString().isNotEmpty()
                && button10.text.toString().isNotEmpty()
                && button11.text.toString().isNotEmpty()
                && button12.text.toString().isNotEmpty()
                && button20.text.toString().isNotEmpty()
                && button21.text.toString().isNotEmpty()
                && button22.text.toString().isNotEmpty()
        ) {
            Toast.makeText(this,"It's a tie", Toast.LENGTH_LONG).show()
            resetButtons()
        }
    }

    override fun onClick(v: View?) {

    }
}